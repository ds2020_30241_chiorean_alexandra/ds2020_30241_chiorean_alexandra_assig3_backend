﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Server.DAL;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc();
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                       .WithMethods("POST, OPTIONS")
                       .AllowAnyHeader()
                       .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
            }));
            /*services.AddDbContext<ApplicationContext>(options =>
               options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));*/
            var builder = new PostgreSqlConnectionStringBuilder("postgres://gggoqhfbgqxqxl:8ca66be4270f3140fae52e840c43a142e6d7278ec3cfab6952c9d5d0fdcf6be5@ec2-54-82-208-124.compute-1.amazonaws.com:5432/dfqvdan113ilgt");
            Console.WriteLine("DEBUG CS" + builder.ConnectionString);

            services.AddDbContext<ApplicationContext>(options =>
                options.UseNpgsql(builder.ConnectionString));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddAutoMapper(typeof(Startup));
            services.AddIdentity<UserAccount, IdentityRole>(options =>

            {
                options.User.RequireUniqueEmail = true;

            })
               .AddRoleManager<RoleManager<IdentityRole>>()
               .AddEntityFrameworkStores<ApplicationContext>()
               .AddDefaultTokenProviders();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseGrpcWeb();
            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GreeterService>().EnableGrpcWeb().RequireCors("AllowAll");
                endpoints.MapGrpcService<MedicationProtoService>().EnableGrpcWeb().RequireCors("AllowAll");
                endpoints.MapGrpcService<AuthenticationService>().EnableGrpcWeb().RequireCors("AllowAll");
                endpoints.MapGrpcService<InTakeMedicationService>().EnableGrpcWeb().RequireCors("AllowAll");
            });
        }
    }
    
}
