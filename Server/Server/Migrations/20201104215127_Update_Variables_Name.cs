﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class Update_Variables_Name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_medicationPlanId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "medicationPlanId",
                table: "MedicationStrategy",
                newName: "MedicationPlanId");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_medicationPlanId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_MedicationPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy",
                column: "MedicationPlanId",
                principalTable: "MedicationPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "MedicationPlanId",
                table: "MedicationStrategy",
                newName: "medicationPlanId");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_MedicationPlanId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_medicationPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_medicationPlanId",
                table: "MedicationStrategy",
                column: "medicationPlanId",
                principalTable: "MedicationPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
