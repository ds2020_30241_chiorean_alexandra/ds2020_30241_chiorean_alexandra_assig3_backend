﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class ChangeToUpper : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationPlans_Pacients_patientId",
                table: "MedicationPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_Medications_medicationId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "medicationId",
                table: "MedicationStrategy",
                newName: "MedicationId");

            migrationBuilder.RenameColumn(
                name: "inTake",
                table: "MedicationStrategy",
                newName: "InTake");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_medicationId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_MedicationId");

            migrationBuilder.RenameColumn(
                name: "startDate",
                table: "MedicationPlans",
                newName: "StartDate");

            migrationBuilder.RenameColumn(
                name: "patientId",
                table: "MedicationPlans",
                newName: "PatientId");

            migrationBuilder.RenameColumn(
                name: "endDate",
                table: "MedicationPlans",
                newName: "EndDate");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationPlans_patientId",
                table: "MedicationPlans",
                newName: "IX_MedicationPlans_PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationPlans_Pacients_PatientId",
                table: "MedicationPlans",
                column: "PatientId",
                principalTable: "Pacients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_Medications_MedicationId",
                table: "MedicationStrategy",
                column: "MedicationId",
                principalTable: "Medications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationPlans_Pacients_PatientId",
                table: "MedicationPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_Medications_MedicationId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "MedicationId",
                table: "MedicationStrategy",
                newName: "medicationId");

            migrationBuilder.RenameColumn(
                name: "InTake",
                table: "MedicationStrategy",
                newName: "inTake");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_MedicationId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_medicationId");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "MedicationPlans",
                newName: "startDate");

            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "MedicationPlans",
                newName: "patientId");

            migrationBuilder.RenameColumn(
                name: "EndDate",
                table: "MedicationPlans",
                newName: "endDate");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationPlans_PatientId",
                table: "MedicationPlans",
                newName: "IX_MedicationPlans_patientId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationPlans_Pacients_patientId",
                table: "MedicationPlans",
                column: "patientId",
                principalTable: "Pacients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_Medications_medicationId",
                table: "MedicationStrategy",
                column: "medicationId",
                principalTable: "Medications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
