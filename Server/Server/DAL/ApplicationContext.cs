﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Server.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DAL
{
    public class ApplicationContext:IdentityDbContext<UserAccount, IdentityRole, string>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {

        }
        public DbSet<MedicationPlan> MedicationPlans { get; set; }
        public DbSet<Patient> Pacients { get; set; }
        public DbSet<Medication> Medications { get; set; }
        public DbSet<MedicationStrategy> MedicationStrategy { get; set; }
        public DbSet<MedicationIntake> MedicationIntake { get; set; }
    }
}
