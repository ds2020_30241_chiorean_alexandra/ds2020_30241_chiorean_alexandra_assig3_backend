﻿using Server.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DAL
{
    public class UnitOfWork:IUnitOfWork
    {
        private ApplicationContext _context;
        private MedicationPlanRepository _medPlanRepo;
        private PatientRepository _patientRepo;
        private MedicationIntakeRepository _medicationIntakeRepo;
        private MedicationRepository _medicationRepo;
        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
        }
        public MedicationPlanRepository MedicationPlanRepo
        {
            get
            {

                if (this._medPlanRepo == null)
                {
                    this._medPlanRepo = new MedicationPlanRepository(this._context);
                }
                return _medPlanRepo;
            }
        }

        public PatientRepository PatientRepo
        {
            get
            {

                if (this._patientRepo == null)
                {
                    this._patientRepo = new PatientRepository(this._context);
                }
                return _patientRepo;
            }
        }
        public MedicationIntakeRepository MedicationIntakeRepo
        {
            get
            {

                if (this._medicationIntakeRepo == null)
                {
                    this._medicationIntakeRepo = new MedicationIntakeRepository(this._context);
                }
                return _medicationIntakeRepo;
            }
        }

        public MedicationRepository MedicationRepo
        {
            get
            {

                if (this._medicationRepo == null)
                {
                    this._medicationRepo = new MedicationRepository(this._context);
                }
                return _medicationRepo;
            }
        }
        public void Save()
        {
            this._context.SaveChanges();
        }
    }
}
