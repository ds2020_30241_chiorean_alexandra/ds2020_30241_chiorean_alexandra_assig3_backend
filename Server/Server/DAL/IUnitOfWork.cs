﻿using Server.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DAL
{
    public interface IUnitOfWork
    {
        public MedicationPlanRepository MedicationPlanRepo { get; }
        public PatientRepository PatientRepo { get; }
        public MedicationIntakeRepository MedicationIntakeRepo { get; }
        public MedicationRepository MedicationRepo { get; }
        public void Save();
    }
}
