﻿using Server.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class MedicationRepository :GenericRepository<Medication>
    {
        public MedicationRepository(ApplicationContext context) : base(context)
        {
         
            this.context = context;
        }
    }
}
