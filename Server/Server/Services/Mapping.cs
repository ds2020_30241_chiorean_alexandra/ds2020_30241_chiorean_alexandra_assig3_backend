﻿using AutoMapper;
using Server.Models.Entities;
using Server.Models.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MedicationPlan, MedicationPlanMapper>();
            CreateMap<MedicationStrategy, MedicationStrategyMapper>();
            CreateMap<Medication, MedicationMapper>();
            CreateMap<Patient, PatientMapper>();
            
        }
    }
}
