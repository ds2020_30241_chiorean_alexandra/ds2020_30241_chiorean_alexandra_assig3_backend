﻿using AutoMapper;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Server.Models.Mappers;
using Server.Models.Entities;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;

namespace Server
{
    public class MedicationProtoService : MedicationProto.MedicationProtoBase
    {
        private readonly ILogger<MedicationProtoService> _logger;
        private readonly IUnitOfWork _uow;
        private IMapper _mapper;

        public MedicationProtoService(ILogger<MedicationProtoService> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _uow = unitOfWork;
            _mapper = mapper;
        }
        public override async Task<MedicationPlanList> GetMedicationPlans(PatientMedicationRequest request, ServerCallContext context)
        {
            string userId = request.UserId;
            Expression<Func<Patient, bool>> queryPatient = patient => patient.UserId == userId;
            var patient =  this._uow.PatientRepo.Get(queryPatient).SingleOrDefault();
            
            Expression<Func<Server.Models.Entities.MedicationPlan, bool>> query = plan => plan.PatientId == patient.Id && plan.StartDate.Date <= DateTime.Now.Date && plan.EndDate.Date >= DateTime.Now.Date ;
            var result = this._uow.MedicationPlanRepo.Get(query, null, "MedsList,MedsList.Medication");
            var maps = this._mapper.Map<IEnumerable<MedicationPlanMapper>>(result);
            
            var smth = new MedicationPlanList();
            foreach (var plan in maps){
                foreach (var medStrategy in plan.MedsList)
                {
                    var item = new MedicationInProto { Name = medStrategy.Medication.Name, Dosage = medStrategy.Medication.Dosage, SideEffects = medStrategy.Medication.SideEffects };
                     string[] intervals = medStrategy.InTake.Split(';');
                    foreach(var interval in intervals)
                    {
                        string[] hours = interval.Trim().Split('-');
                        MedicationStrategyProto strategy = new MedicationStrategyProto { Medication = item, StartTime = hours[0], EndTime = hours[1]};
                        smth.Plan.Add(strategy);
                    }
                   
                } 
            }

            return await Task.FromResult(smth);
        }

    }
}
