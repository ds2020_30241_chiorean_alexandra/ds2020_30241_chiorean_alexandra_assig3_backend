﻿using AutoMapper;
using Grpc.Core;
using Server.DAL;
using Server.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Server
{
    public class InTakeMedicationService :InTakeMedication.InTakeMedicationBase
    {
        private readonly IUnitOfWork _uow;
        private IMapper _mapper;

        public InTakeMedicationService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _uow = unitOfWork;
            _mapper = mapper;
        }
        public override async Task<MedicationIntakeSaved> MedicationTaken(InTakeMedicationRequest request, ServerCallContext context)  
        {
            string userId = request.UserId;
            Expression<Func<Patient, bool>> queryPatient = patient => patient.UserId == userId;
            var patient = this._uow.PatientRepo.Get(queryPatient).SingleOrDefault();

            Expression<Func<Medication, bool>> queryMedication = med => med.Name == request.MedicationName && med.Dosage == request.MedicationDosage;
            var med = this._uow.MedicationRepo.Get(queryMedication).SingleOrDefault();

            var inTake = new MedicationIntake { PatientId = patient.Id, MedicationId = med.Id, intakeTime = request.TakenTime.ToDateTime(), Status = request.Status };
            _uow.MedicationIntakeRepo.Insert(inTake);
            _uow.Save();

            return new MedicationIntakeSaved { Saved = true };
        }
    }
}
