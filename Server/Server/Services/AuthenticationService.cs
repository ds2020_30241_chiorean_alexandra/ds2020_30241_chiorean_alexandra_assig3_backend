﻿using Grpc.Core;
using Microsoft.AspNetCore.Identity;
using Server.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server
{
    public class AuthenticationService:Authentication.AuthenticationBase
    {
        private readonly UserManager<UserAccount> _userManager;

        public AuthenticationService(UserManager<UserAccount> userManager){
            _userManager = userManager;        
        }

        public override async Task<LoginRespnse> LoginPatient(PatientLoginRequest request, ServerCallContext context)
        {
            var email = request.Email;
            var password = request.Password;
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null && await _userManager.CheckPasswordAsync(user, password))
            {
                return await Task.FromResult(new LoginRespnse { StatusCode = 200, UserId = user.Id});
            }
            return await Task.FromResult(new LoginRespnse { StatusCode = 403, UserId = null });
        }
    }
}
