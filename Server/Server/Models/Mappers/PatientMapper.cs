﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Mappers
{
    public class PatientMapper
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string MedicalRecord { get; set; }
        public IEnumerable<MedicationPlanMapper> MedicationPlans { get; set; }
    }
}
