﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Mappers
{
    public class MedicationMapper
    {
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public float Dosage { get; set; }
    }
}
