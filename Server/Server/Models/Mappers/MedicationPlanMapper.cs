﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Mappers
{
    public class MedicationPlanMapper
    {
        public IEnumerable<MedicationStrategyMapper> MedsList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
