﻿using Server.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Mappers
{
    public class MedicationStrategyMapper
    {
        public MedicationMapper Medication { get; set; }
        public string InTake { get; set; }
    }
}
