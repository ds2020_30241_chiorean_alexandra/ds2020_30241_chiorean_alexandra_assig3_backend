﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Entities
{
    public class MedicationStrategy
    {
        [Key]
        public int Id { get; set; }
        public int MedicationId { get; set; }
        public Medication Medication { get; set; }
        public string InTake { get; set; }
        public MedicationPlan MedicationPlan { get; set; }
        public int MedicationPlanId { get; set; }
    }
}
