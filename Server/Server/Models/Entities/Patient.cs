﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Server.Models.Entities
{
    public class Patient
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string MedicalRecord { get; set; }
        public UserAccount User { get; set; }
        public string UserId { get; set; }
        public int DoctorId { get; set; }
        public int CaregiverId { get; set; }
        public IEnumerable<MedicationPlan> MedicationPlans { get; set; }
    }
}
