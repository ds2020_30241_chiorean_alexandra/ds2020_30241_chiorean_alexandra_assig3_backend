﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Server.Models.Entities
{
    public class MedicationPlan
    {
        [Key]
        public int Id { get; set; }
        public IEnumerable<MedicationStrategy> MedsList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}
