﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.Entities
{
    public class MedicationIntake
    {
        public Guid Id { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public int MedicationId { get; set; }
        public Medication Medication { get; set; }
        public DateTime intakeTime { get; set; }
        public string Status { get; set; }
    }
}
